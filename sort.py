#!/usr/bin/env python3

import pickle
from pprint import pprint

with open('disponibilita.pkl', 'rb') as inFile:
    disponibilita = pickle.load(inFile)

disponibilita = sorted(
    disponibilita,
    key=lambda x: sorted(x['date'])[0]
)

for d in disponibilita:
    print('Disponibilità per ', d['posto'])
    print(', '.join([data.strftime('%d/%m/%Y') for data in d['date']]))
    print()
