#!/usr/bin/env python3

from requests import Session
from datetime import datetime
from bs4 import BeautifulSoup
import pickle

data = {
    'name_email': 'lefnk@akjemf.it'
}

s = Session()
s.headers.update(
    {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'
    }
)

s.get('https://ulss6euganea.prenotami.cloud/')
r = s.post('https://ulss6euganea.prenotami.cloud/mobile/gest_inizio.html', data=data)

if r.status_code != 200:
    print('AAAAAAAAAAAAAA')
    exit(1)

soup = BeautifulSoup(r.text, 'html.parser')
posti = soup.find_all('input', attrs={'type': 'submit', 'class': 'input_cella_submit'})
posti = list(filter(lambda x: x.get('name') != 'name_indietro', posti))
disponibilita = []

for posto in posti:
    data = {
        posto.get('name'): posto.get('value').replace(' ', '+'),
        f'id_istanza_p': posto.get('name').strip('name_')
    }

    r = s.post('https://ulss6euganea.prenotami.cloud/mobile/gest_sedi.html', data=data)

    soup = BeautifulSoup(r.text, 'html.parser')
    servizi = soup.find_all('input', attrs={'type': 'submit', 'class': 'input_cella_submit'})

    prelievo = list(filter(lambda x: x.get('value') == 'Prelievo', servizi))

    if len(prelievo) == 0:
        print(f'Nessun match per "Prelievo" ({posto.get("name")})')
        continue
    elif len(prelievo) > 1:
        print(f'Più di un match per "Prelievo" ({posto.get("name")})')
        continue
    else:
        prelievo = prelievo[0]

    # Chiedo le date disponibili
    data = {
        'name_CODA_01': 'Prelievo',
        'id_coda_cremagliera_p': 'CODA_01'
    }

    r = s.post('https://ulss6euganea.prenotami.cloud/mobile/gest_servizi.html', data=data)

    soup = BeautifulSoup(r.text, 'html.parser')
    date = soup.find_all('input', attrs={'type': 'submit', 'class': 'input_cella_submit'})
    date = list(filter(lambda x: x.get('id').startswith('id_data'), date))

    if len(date) == 0:
        print(f'Nessuna data disponibile ({posto.get("name")})')
        continue

    date = [
        datetime.strptime(d.get('id'), 'id_data_%Y%m%d') for d in date
    ]

    print('==========================================')
    print(posto.get('value'))
    print(date)
    print('==========================================')

    disponibilita.append({
        'posto': posto.get('value'),
        'date': date
    })

with open('disponibilita.pkl', 'wb') as outFile:
    pickle.dump(disponibilita, outFile)
